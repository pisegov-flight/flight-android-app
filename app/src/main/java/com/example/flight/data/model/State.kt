package com.example.flight.data.model

data class State(
    val lightingIsOn: Boolean,
    val scheduleIsOn: Boolean,
    val lightingStartTime: Int,
    val lightingStopTime: Int,
)