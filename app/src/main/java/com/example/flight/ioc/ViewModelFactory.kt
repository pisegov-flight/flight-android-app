package com.example.flight.ioc

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.flight.data.StateKeeper
import com.example.flight.data.repository.StateRepository
import com.example.flight.ui.stateholders.StateViewModel

class ViewModelFactory(
    private val stateRepository: StateRepository,
    private val stateKeeper: StateKeeper
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T = when (modelClass) {
        StateViewModel::class.java -> StateViewModel(
            stateRepository,
            stateKeeper
        )

        else -> throw IllegalArgumentException("${modelClass.simpleName} cannot be provided.")
    } as T
}
