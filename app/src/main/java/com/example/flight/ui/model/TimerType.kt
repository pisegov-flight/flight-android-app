package com.example.flight.ui.model

sealed class TimerType {
    override fun toString(): String {
        return this.javaClass.simpleName
    }

    object Start : TimerType()

    object Stop : TimerType()
}
