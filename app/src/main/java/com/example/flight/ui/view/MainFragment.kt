package com.example.flight.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.flight.App
import com.example.flight.databinding.FragmentMainBinding
import com.example.flight.ioc.MainFragmentComponent
import com.example.flight.ioc.MainFragmentViewComponent
import com.example.flight.ui.stateholders.StateViewModel

class MainFragment : Fragment() {

    private val applicationComponent
        get() = App.get(requireContext()).applicationComponent
    private lateinit var fragmentComponent: MainFragmentComponent
    private var fragmentViewComponent: MainFragmentViewComponent? = null

    private val viewModel: StateViewModel by viewModels { applicationComponent.viewModelFactory }

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragmentComponent = MainFragmentComponent(
            applicationComponent,
            fragment = this,
            viewModel = viewModel,
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentMainBinding.inflate(inflater, container, false)

        val view = binding.root
        fragmentViewComponent = MainFragmentViewComponent(
            fragmentComponent,
            binding,
            viewLifecycleOwner
        ).apply {
            stateViewController.setupViews()
        }

        fragmentComponent.applicationComponent.poller.start()

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

        fragmentComponent.applicationComponent.poller.stop()
    }
}