package com.example.flight.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.flight.data.model.State

class StateKeeper {
    private var _state = MutableLiveData<State>(
        State(
            lightingStartTime = 800,
            lightingStopTime = 2200,
            lightingIsOn = false,
            scheduleIsOn = false
        )
    )
    val state: LiveData<State>
        get() = _state

    fun updateState(state: State) {
        _state.postValue(state)
    }
}
