package com.example.flight.ioc

import android.util.Log
import com.example.flight.BuildConfig
import com.example.flight.data.NetworkDataSource
import com.example.flight.data.StateKeeper
import com.example.flight.data.network.retrofit.model.StateResponse
import com.example.flight.data.repository.StateRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.decodeFromString
import me.nikhilchaudhari.simplepoller.Poller

class ApplicationComponent {
    @OptIn(ExperimentalSerializationApi::class)
    private val dataSource = NetworkDataSource()
    private val stateKeeper = StateKeeper()

    @OptIn(ExperimentalSerializationApi::class)
    private val stateRepository = StateRepository(dataSource)
    val poller = getPollerBuilder().build()

    val viewModelFactory = ViewModelFactory(stateRepository, stateKeeper)

    private fun getPollerBuilder(): Poller.Builder {
        return Poller.Builder()
            .get(url = "${BuildConfig.BASE_URL}/state")
            .setInfinitePoll(true)
            .setIntervals(50, 60000, 2, 1000)
            .setDispatcher(Dispatchers.IO)
            .onResponse {
                val obj = Json.decodeFromString<StateResponse>(it.text).toState()
                Log.d("Network message", it.text)
                stateKeeper.updateState(obj)
            }.onError {
                it?.message?.let { message ->
                    Log.e(
                        "Network Error",
                        message
                    )
                }
            }
    }
}
