package com.example.flight.data.network.retrofit.model

import com.example.flight.data.model.State
import kotlinx.serialization.Serializable

@Serializable
data class StateRequest(
    val lightingIsOn: Boolean,
    val scheduleIsOn: Boolean,
    val lightingStartTime: Int,
    val lightingStopTime: Int,
)

fun State.toStateRequest(): StateRequest {
    return StateRequest(
        lightingIsOn = this.lightingIsOn,
        scheduleIsOn = this.scheduleIsOn,
        lightingStartTime = this.lightingStartTime,
        lightingStopTime = this.lightingStopTime,
    )
}
