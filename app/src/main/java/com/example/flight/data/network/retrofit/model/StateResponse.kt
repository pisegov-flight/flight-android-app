package com.example.flight.data.network.retrofit.model

import com.example.flight.data.model.State
import kotlinx.serialization.Serializable

@Serializable
data class StateResponse(
    val lightingIsOn: Boolean,
    val scheduleIsOn: Boolean,
    val lightingStartTime: Int,
    val lightingStopTime: Int,
) {
    fun toState(): State {
        return State(
            lightingIsOn = this.lightingIsOn,
            scheduleIsOn = this.scheduleIsOn,
            lightingStartTime = this.lightingStartTime,
            lightingStopTime = this.lightingStopTime,
        )
    }
}