package com.example.flight.ioc

import androidx.lifecycle.LifecycleOwner
import com.example.flight.databinding.FragmentMainBinding
import com.example.flight.ui.view.StateViewController


class MainFragmentViewComponent(
    fragmentComponent: MainFragmentComponent,
    viewBinding: FragmentMainBinding,
    lifecycleOwner: LifecycleOwner,
) {
    val stateViewController = StateViewController(
        fragmentComponent.fragment.requireActivity(),
        viewBinding,
        lifecycleOwner,
        fragmentComponent.viewModel,
    )
}
