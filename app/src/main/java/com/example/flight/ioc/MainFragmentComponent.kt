package com.example.flight.ioc

import com.example.flight.ui.stateholders.StateViewModel
import com.example.flight.ui.view.MainFragment

class MainFragmentComponent(
    val applicationComponent: ApplicationComponent,
    val fragment: MainFragment,
    val viewModel: StateViewModel
)