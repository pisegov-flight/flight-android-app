package com.example.flight.data.network.retrofit

import com.example.flight.data.network.retrofit.model.StateRequest
import com.example.flight.data.network.retrofit.model.StateResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface StateAPI {
    @POST("/state")
    suspend fun pushState(
        @Body state: StateRequest
    ): StateResponse
}
