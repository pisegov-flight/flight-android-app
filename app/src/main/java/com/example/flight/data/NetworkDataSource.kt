package com.example.flight.data

import com.example.flight.data.model.State
import com.example.flight.data.network.retrofit.RetrofitModule
import com.example.flight.data.network.retrofit.model.StateResponse
import com.example.flight.data.network.retrofit.model.toStateRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
class NetworkDataSource {
    suspend fun pushState(state: State): StateResponse = withContext(Dispatchers.IO) {
        return@withContext RetrofitModule.stateAPI.pushState(state.toStateRequest())
    }
}