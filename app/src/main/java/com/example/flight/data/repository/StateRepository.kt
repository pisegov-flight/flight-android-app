package com.example.flight.data.repository

import com.example.flight.data.NetworkDataSource
import com.example.flight.data.StateKeeper
import com.example.flight.data.model.State
import com.example.flight.data.network.retrofit.NetworkResult
import com.example.flight.data.network.retrofit.model.StateResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.ExperimentalSerializationApi
import retrofit2.HttpException
import java.io.IOException

class StateRepository @OptIn(ExperimentalSerializationApi::class) constructor(
    private val dataSource: NetworkDataSource
) {
    @OptIn(ExperimentalSerializationApi::class)
    suspend fun updateState(state: State): NetworkResult<StateResponse> {
        return safeApiCall { dataSource.pushState(state) }
    }

    private suspend fun <T : Any> safeApiCall(
        apiCall: suspend () -> T
    ): NetworkResult<T> {
        return withContext(Dispatchers.IO) {
            try {
                NetworkResult.Success(apiCall.invoke())
            } catch (error: Throwable) {
                when (error) {
                    is IOException ->
                        NetworkResult.BackendConnectionError

                    is HttpException -> {
                        val response = error.response()?.errorBody()?.source().toString()
                        NetworkResult.MicrocontrollerConnectionError(response)
                    }

                    else -> NetworkResult.MicrocontrollerConnectionError("Undefined error")
                }
            }
        }
    }
}
