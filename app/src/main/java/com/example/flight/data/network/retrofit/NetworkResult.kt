package com.example.flight.data.network.retrofit

sealed class NetworkResult<out T : Any> {
    data class Success<out T : Any>(val data: T) : NetworkResult<T>()
    data class MicrocontrollerConnectionError(val message: String) : NetworkResult<Nothing>()
    object BackendConnectionError : NetworkResult<Nothing>()
}