package com.example.flight.ui.view

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Context
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import com.example.flight.R
import com.example.flight.data.model.State
import com.example.flight.databinding.FragmentMainBinding
import com.example.flight.ui.model.TimerType
import com.example.flight.ui.stateholders.StateViewModel
import java.text.DecimalFormat
import java.text.NumberFormat


class StateViewController(
    private val activity: Activity,
    private val viewBinding: FragmentMainBinding,
    private val lifecycleOwner: LifecycleOwner,
    private val viewModel: StateViewModel
) {

    private val currentState
        get() = viewModel.stateLiveData.value!!

    fun setupViews() {
        setupStateObserver()
        setupLightingToggleButton()
        setupTimerTextView(viewBinding.tvLightingStartTime, TimerType.Start)
        setupTimerTextView(viewBinding.tvLightingStopTime, TimerType.Stop)
        setupTimerSwitch()
    }

    private fun setupTimerSwitch() {
        val switch = viewBinding.switchLightingByTimer

        switch.setOnCheckedChangeListener { buttonview, isChecked ->
            viewModel.updateState(currentState.copy(scheduleIsOn = isChecked))
        }
    }

    private fun setupStateObserver() {
        viewModel.stateLiveData.observe(lifecycleOwner) { state ->
            setNewState(state)
        }

        viewModel.errorLiveData.observe(lifecycleOwner) { error ->
            error?.let {
                Toast.makeText(activity as Context, error, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setupLightingToggleButton() {
        setLightingState(lightingIsOn = false)
        viewBinding.btnToggleLighting.setOnClickListener {
            val lightingIsOn = currentState.lightingIsOn
            viewModel.updateState(currentState.copy(lightingIsOn = !lightingIsOn))
        }
    }

    private fun setupTimerTextView(textView: TextView, timerType: TimerType) {

        textView.setOnClickListener {
            // to show previous time on a new pick
            val previousTime = textView.text.split(":").map { it.toInt() }

            val timePicker = TimePickerDialog(
                activity as Context, { _, selectedHour, selectedMinute ->
                    viewModel.updateTimer(
                        selectedHour * 100 + selectedMinute,
                        timerType
                    )
                },
                previousTime[0], // hour of day
                previousTime[1], // minute
                true // is 24 hour view
            )
            timePicker.setTitle("Select ${timerType.toString().lowercase()} time")
            timePicker.show()
        }
    }

    private fun makeTimeString(time: Int): String {
        val hour = time / 100
        val minute = time % 100

        // need for output in format like 08:00 instead of 8:0
        val f: NumberFormat = DecimalFormat("00")
        return "${f.format(hour)}:${f.format(minute)}"
    }

    private fun setNewState(state: State) {
        setLightingState(state.lightingIsOn)
        setTimerSwitchState(state.scheduleIsOn)
        viewBinding.tvLightingStartTime.text = makeTimeString(state.lightingStartTime)
        viewBinding.tvLightingStopTime.text = makeTimeString(state.lightingStopTime)
    }

    private fun setLightingState(lightingIsOn: Boolean) {
        val button = viewBinding.btnToggleLighting
        val messageTextView = viewBinding.tvLightingIsOn

        if (lightingIsOn) {
            messageTextView.text = "Lighting is on"
            button.setBackgroundColor(activity.resources.getColor(androidx.appcompat.R.color.material_deep_teal_200))
            button.text = "off"
        } else {
            messageTextView.text = "Lighting is off"
            button.setBackgroundColor(activity.resources.getColor(R.color.purple))
            button.text = "on"
        }
    }

    private fun setTimerSwitchState(lightingByTimerIsOn: Boolean) {
        val switch = viewBinding.switchLightingByTimer
        switch.isChecked = lightingByTimerIsOn
    }
}
