package com.example.flight.ui.stateholders

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.flight.data.StateKeeper
import com.example.flight.data.model.State
import com.example.flight.data.network.retrofit.NetworkResult
import com.example.flight.data.repository.StateRepository
import com.example.flight.ui.model.TimerType
import kotlinx.coroutines.launch

class StateViewModel(
    private val stateRepository: StateRepository,
    private val stateKeeper: StateKeeper
) : ViewModel() {
    val stateLiveData
        get() = stateKeeper.state

    private val _errorLiveData = MutableLiveData<String?>()
    val errorLiveData: LiveData<String?>
        get() = _errorLiveData

    fun updateState(state: State) {
        viewModelScope.launch {
            when (val result = stateRepository.updateState(state)) {
                is NetworkResult.BackendConnectionError -> {
                    _errorLiveData.postValue("BackendConnectionError")
                }

                is NetworkResult.MicrocontrollerConnectionError -> {
                    _errorLiveData.postValue(result.message)
                }

                is NetworkResult.Success -> {
                    _errorLiveData.postValue(null)
                    stateKeeper.updateState(state)
                }
            }
        }
    }

    fun updateTimer(time: Int, timerType: TimerType) {
        when (timerType) {
            is TimerType.Start -> {
                val state = stateLiveData.value
                state?.let { updateState(it.copy(lightingStartTime = time)) }
            }

            is TimerType.Stop -> {
                val state = stateLiveData.value
                state?.let { updateState(it.copy(lightingStopTime = time)) }
            }
        }
    }
}
